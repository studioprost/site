package main

import (
	"fmt"
	"log"
	"net/http"
)

// handler will process incoming requests and write "Under construction" centered on the page.
func handler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, `<html>
<head>
<title>Under Construction</title>
<style>
body {
    height: 100vh;
    margin: 0;
    display: flex;
    align-items: center;
    justify-content: center;
    font-family: Arial, sans-serif;
}
</style>
</head>
<body>
    <div>Under construction</div>
</body>
</html>`)
}

func main() {
	// Set the handler function for the root route
	http.HandleFunc("/", handler)

	// Start the server on port 8080 and log if there is an error
	log.Println("Server is running on http://localhost:8080")
	log.Fatal(http.ListenAndServe(":8080", nil))
}
