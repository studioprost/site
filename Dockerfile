FROM golang:1.22 as builder

WORKDIR /app

COPY go.mod ./
# COPY go.sum ./

RUN go mod download

COPY . .

RUN CGO_ENABLED=0 GOOS=linux go build -o /wiki-go

FROM scratch

COPY --from=builder /wiki-go /wiki-go

EXPOSE 8080

CMD ["/wiki-go"]
